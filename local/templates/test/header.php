<?php

use Bitrix\Main\Page\Asset;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
?>
<!DOCTYPE html>
<html lang='ru'>
<head>
    <title><?php $APPLICATION->ShowTitle(); ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <?php $APPLICATION->ShowHead(); ?>
    <?php CJSCore::Init(['jquery3']); ?>
    <?php Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/asset/bootstrap/bootstrap.min.css'); ?>
    <?php Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/asset/bootstrap/bootstrap.min.js'); ?>
</head>
<body>
<div id="panel"><?php $APPLICATION->ShowPanel(); ?></div>
