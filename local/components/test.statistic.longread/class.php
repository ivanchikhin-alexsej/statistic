<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SystemException;
use Bitrix\Main\Loader;

class TestStatisticLongread extends CBitrixComponent
{
    public function executeComponent(): void
    {
        try {
            $this->checkModules();
            $this->getResult();
        } catch (SystemException $e) {
            ShowError($e->getMessage());
        }
    }

    public function onIncludeComponentLang(): void
    {
        Loc::loadMessages(__FILE__);
    }

    protected function checkModules(): void
    {
        if (!Loader::includeModule('highloadblock')) {
            throw new SystemException(Loc::getMessage('HLBLOCK_MODULE_NOT_INSTALLED'));
        }
    }

    protected function getResult(): void
    {
        $this->IncludeComponentTemplate();
    }
}

