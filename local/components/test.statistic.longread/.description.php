<?php

use Bitrix\Main\Localization\Loc;

$arComponentDescription = [
    'NAME' => Loc::getMessage('COMPONENT_NAME'),
    'DESCRIPTION' => Loc::getMessage('COMPONENT_DESC'),
    'SORT' => 10,
    'COMPLEX' => 'N',
    'PATH' => [
        'ID' => 'content',
    ],
];
