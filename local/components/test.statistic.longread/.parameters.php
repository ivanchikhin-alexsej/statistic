<?php

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

if (!Loader::includeModule('highloadblock')) {
    return;
}

$hlBlockIterator = HighloadBlockTable::getList([
    'select' => ['ID', 'NAME'],
    'order' => ['ID' => 'ASC'],
]);

$hlBlockList = [];

while ($hlBlock = $hlBlockIterator->Fetch()) {
    $hlBlockList[$hlBlock['ID']] = '[' . $hlBlock['ID'] . '] - ' . $hlBlock['NAME'];
}

$arComponentParameters = [
    'GROUPS' => [
        'MAIN' => [
            'NAME' => Loc::getMessage('MAIN_SETTINGS'),
        ],
    ],
    'PARAMETERS' => [
        'HLBLOCK_ID' => [
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('SELECT_HLBLOCK'),
            'TYPE' => 'LIST',
            'VALUES' => $hlBlockList,
            'REFRESH' => 'Y',
            'DEFAULT' => '',
            'ADDITIONAL_VALUES' => 'Y',
        ],
    ],
];
