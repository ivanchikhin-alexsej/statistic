$(function() {
    function sendStatistic(data)
    {
        let settings = $('#statistic-collector'),
            idHlBlock = settings.data('hlblock-id'),
            url = settings.data('ajax-url');

        $.post({
            url: url,
            data: {
                idHlBlock: idHlBlock,
                data: data,
            },
            dataType: 'json'
        });
    }

    if (navigator.cookieEnabled === false) {
        console.log('Cookies отключены!');
    } else {
        let countHit = 0,
            dataTimeLoad = Math.floor(new Date().getTime() / 1000),
            clicks = [],
            downloads = [],
            scrollMax = 0,
            data = {};

        // считаем хиты

        if ($.cookie('countHit') == null) {
            $.cookie('countHit', '1');
            countHit = 1;
        } else {
            countHit = Number($.cookie('countHit'));
            countHit = ++countHit;
            $.cookie('countHit', countHit);
        }

        // запоминаем больший scroll

        $(window).on('scroll', function(){
            let s = $(window).scrollTop(),
                d = $(document).height(),
                c = $(window).height();

            let scroll = Math.round((s / (d - c)) * 100);

            if (scroll > scrollMax) {
                scrollMax = scroll;
            }
        })

        // запоминаем клики и ссылки загрузок

        function clickListener(e)
        {
            if (e.target.getAttribute('download') == null) {
                clicks.push(e.target.getAttribute('id'));
            } else {
                downloads.push(e.target.getAttribute('id'));
            }
        }

        document.onclick = clickListener;

        // закрыли страницу - отправляем данные

        window.onbeforeunload = function() {
            data = {
                dataTimeLoad: dataTimeLoad,
                dataTimeUnLoad: Math.floor(new Date().getTime() / 1000),
                clicks: clicks,
                downloads: downloads,
                scrollMax: scrollMax,
                countHit: countHit,
            }

            sendStatistic(data);

            return true;
        };

        $('#action').click(function(e){
            e.preventDefault();

            data = {
                dataTimeLoad: dataTimeLoad,
                dataTimeUnLoad: Math.floor(new Date().getTime() / 1000),
                clicks: clicks,
                downloads: downloads,
                scrollMax: scrollMax,
                countHit: countHit,
            }

            sendStatistic(data);
        });
    }
})