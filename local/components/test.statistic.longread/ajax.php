<?php

use Bitrix\Main\Application;
use Bitrix\Main\Engine\CurrentUser;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Test\Statistic\Service;
use Test\Statistic\Statistic;

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

if (!Loader::includeModule('highloadblock')) {
    $data['error'] = Loc::getMessage('HLBLOCK_MODULE_NOT_INSTALLED');
    echo json_encode($data);
    exit();
}

if (!Loader::includeModule('test.statistic')) {
    $data['error'] = Loc::getMessage('STATISTIC_MODULE_NOT_INSTALLED');
    echo json_encode($data);
    exit();
}

$currentUser = CurrentUser::get()->getId();

if (!$currentUser) {
    $data['error'] = Loc::getMessage('USER_NOT_FOUND');
    echo json_encode($data);
    exit();
}

$request = Application::getInstance()->getContext()->getRequest();

$idHlBlock = $request->get('idHlBlock');
$dataSend = $request->get('data');

$data = [];

$objStatistic = new Statistic($idHlBlock, $currentUser);
$objStatistic->set($dataSend);

$data['success'] = Service::add($objStatistic);

echo json_encode($data);
