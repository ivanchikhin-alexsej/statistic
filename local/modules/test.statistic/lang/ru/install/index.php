<?php

$MESS['MODULE_NAME']              = 'Статистика';
$MESS['MODULE_DESCRIPTION']       = 'Собирает статистику действий на странице авторизованного пользователя в HL';
$MESS['MODULE_INSTALL_TITLE']     = 'Установка модуля';
$MESS['MODULE_UNINSTALL_TITLE']   = 'Удаление модуля';