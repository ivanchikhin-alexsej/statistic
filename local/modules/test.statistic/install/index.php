<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

class Test_Statistic extends CModule
{
    public function __construct()
    {
        include_once(__DIR__ . '/version.php');
        $this->MODULE_ID = 'test.statistic';
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME = Loc::getMessage('MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('MODULE_DESCRIPTION');
    }

    public function doInstall(): void
    {
        global $APPLICATION;

        ModuleManager::registerModule($this->MODULE_ID);

        $APPLICATION->includeAdminFile(
            Loc::getMessage('MODULE_INSTALL_TITLE') . ' «' . Loc::getMessage('MODULE_NAME') . '»',
            __DIR__ . '/step.php'
        );
    }

    public function doUninstall(): void
    {
        global $APPLICATION;

        ModuleManager::unRegisterModule($this->MODULE_ID);

        $APPLICATION->includeAdminFile(
            Loc::getMessage('MODULE_UNINSTALL_TITLE') . ' «' . Loc::getMessage('MODULE_NAME') . '»',
            __DIR__ . '/unstep.php'
        );

    }
}
