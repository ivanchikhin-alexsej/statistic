<?php

namespace Test\Statistic;

use Bitrix\Highloadblock\HighloadBlockTable;

class Service
{
    public static function add(Statistic $objStatistic): bool
    {
        $hlBlock = HighloadBlockTable::getById($objStatistic->hlBlockId)->fetch();
        $objEntity = HighloadBlockTable::compileEntity($hlBlock);
        $strEntityDataClass = $objEntity->getDataClass();

        $obResult = $strEntityDataClass::add($objStatistic->get());

        return $obResult->isSuccess();
    }
}
