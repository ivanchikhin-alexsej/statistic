<?php

namespace Test\Statistic;

use Bitrix\Main\Type\DateTime;

class Statistic
{
    public int $hlBlockId;

    private int $currentUserId;

    private array $hlUfValues;

    public function __construct(int $hlBlockId, int $currentUserId)
    {
        $this->hlBlockId = $hlBlockId;
        $this->currentUserId = $currentUserId;
    }

    public function set(array $data): void
    {
        $data['dataTimeLoad'] = DateTime::createFromTimestamp($data['dataTimeLoad']);
        $data['dataTimeLoad'] = $data['dataTimeLoad']->toString();

        $data['dataTimeUnLoad'] = DateTime::createFromTimestamp($data['dataTimeUnLoad']);
        $data['dataTimeUnLoad'] = $data['dataTimeUnLoad']->toString();

        $this->hlUfValues = [
            'UF_USER_ID' => $this->currentUserId,
            'UF_DATATIME_LOAD' => $data['dataTimeLoad'],
            'UF_DATATIME_UNLOAD' => $data['dataTimeUnLoad'],
            'UF_SCROLL' => $data['scrollMax'],
            'UF_DOWNLOAD' => $data['downloads'],
            'UF_CLICK' => $data['clicks'],
            'UF_COUNT_HIT' => $data['countHit'],
        ];
    }

    public function get(): array
    {
        return $this->hlUfValues;
    }
}
